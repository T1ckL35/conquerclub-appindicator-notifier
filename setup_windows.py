#!/usr/bin/env python

from cx_Freeze import setup, Executable
import sys

buildOptions = dict(
  packages = [
    "sqlite3",
  ],
  include_files = [
    "db/schema.sql",
    "ui/img/cc.ico",
    "ui/img/cc2.ico",
  ],
  excludes = [
    "tcl",
    "tkk",
    "_osx_support",
    "bz2",
    "email",
    "unittest",
    "xml"
  ]
)

exe = Executable(
    script="ccnotifier.py",
    base="Win32GUI",
    icon="ui\img\cc.ico",
    targetName="ccnotifier.exe"
)
setup(
    name="ccnotifier.exe",
    version="1.0",
    author="Simon Mann, simon.mann+ccnotifer@gmail.com",
    description="Cross platform desktop notifier for Conquer Club (wwww.conquerclub.com)",
    executables=[exe],
    options = dict( build_exe = buildOptions ),
) 