
import feedparser

class ConquerClubReader:

    def __init__(self):
        pass

    def getRecords(self, config):
        hasResult = False
        resultRecord = [] 

        # TODO: change the url call to use the api.php page. Note, this can be called
        #  real-time too! See here for more details/options.
        # http://www.conquerclub.com/api.php

        # get active games for a username. Allows you to query in real-time and retrieve extra data
        # http://www.conquerclub.com/api.php?mode=gamelist&gs=A&names=Y&p1un=simon.mann
        
        # this extra query can be used to get the map images (thumb, small, large) for the specified map
        # http://www.conquerclub.com/api.php?mode=maplist&mp=<map_name>     # can be comma separated names
        # note, the image path is: http://maps.conquerclub.com/<image_type>
        
        # this gets a single map (can be comma separated) data
        # http://www.conquerclub.com/api.php?mode=gamelist&gn=13594854&names=Y

        path = 'http://www.conquerclub.com/rss.php?username='
        # test path# this gets stats on a single (or comma separated list of) user(s)
        # http://www.conquerclub.com/api.php?mode=player&un=simon.mann

        # path = 'http://localhost/rss.php?'
        if config.user_id.isdigit():
            path = 'http://www.conquerclub.com/rss.php?user_id='
        path = path + config.user_id
        d = feedparser.parse(path)

        for entry in d.entries:
            entry.active = False
            # currently known states = 'READY', 'WAITING', 'PLAYING' and 'ELIMINATED'
            # to debug, change the state below to another listed above
            if 'READY' in entry.title:
                entry.active = True
                hasResult = True
            if (config.show_eliminated == '1') or (('ELIMINATED' not in entry.title) == True):
                resultRecord.append(entry)
        return resultRecord, hasResult
        