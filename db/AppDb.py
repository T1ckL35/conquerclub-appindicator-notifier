import os
import sqlite3

class AppDb:

    con = None

    def find(self, query, values):
        self.createDb()
        cursor = self.con.cursor()
        cursor.execute(query)
        return self.format_all(cursor.fetchall())

    def format_all(self, rows):
        formatted = []
        for row in rows:
            record = self.create()
            record.hydrate(row)
            formatted.append(record)
        return formatted


class AppSqliteDb(AppDb):

    dbFile = 'db/ccnotifier.db'
    schemaFile = 'db/schema.sql'
    con = sqlite3.connect('db/ccnotifier.db')

    def createDb(self):
        if not os.path.getsize(self.dbFile) > 0:
            with open(self.schemaFile, 'rt') as f:
                schema = f.read()
                self.con.executescript(schema)


class ConfigQuery(AppSqliteDb):

    def create(self):
        return ConfigRecord()

    def set_user(self, data):
        for key, value in data.iteritems():
            self.con.execute("INSERT OR IGNORE into config (keyName, keyValue) values ('%s', '%s')" % (key, value))
            self.con.execute("UPDATE config SET keyValue='%s' WHERE keyName='%s'" % (value, key))
            self.con.commit()

    def get_config(self):
        query = "SELECT * FROM config"
        result = self.find(query, None)
        if result:
            return result

    def format_all(self, rows):
        record = self.create()
        for row in rows:
            record.hydrate(row)
        return record


class ConfigRecord:
    user_id = None
    password = None
    show_eliminated = None
    polling_interval = None

    def hydrate(self, row):
        for item in row:
            setattr(self, row[1], row[2])
