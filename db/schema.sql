create table config (
    id 			integer primary key autoincrement not null,
	keyName			text,
	keyValue		text,
    UNIQUE (keyName)
);
