CCNotifier

An app to sit in the Appindicator area (system tray) showing an icon depending on if the specified user has a game ready to play or not. It does this by polling the conquerclub user feed. Running the app opens a config box to add your Conquerclub user id, whether you want to show games you have been eliminated in and the interval in minutes between each check (note, it appears the conquerclub rss feed updates every 5 minutes). When the config details are submitted, the data is stored in an sqlite database and retrieved the next time you run the program.
When a game is ready the conquerclub icon is changed to a ready status (tick) and a system message is displayed. Clicking the appindicator icon brings up a menu of options (show config again, force a game check and quit. It also lists each of the games you are currently in and if you click on one then it will jump you straight to the game. Note, it doesn't actually log you in but I have played with selenium bindings for python and the firefox/chrome webdrivers and it is possible to implement an auto-login and redirect to the game selected.

This app has currently been written in Python 2.7 (I had a 3.3 version running so it should be easy to do) and tested on unity and Gnome Shell on Ubuntu 13.04 64bit and Ubuntu 12.10 64bit as well as on Windows 7 64bit. It should work in earlier versions that run Unity straight out of the box but haven't tried yet.
To get this working in Gnome Shell you will need to install the gnome shell appindicator extension (https://extensions.gnome.org/extension/615/appindicator-support/). Once installed open the Tweak Tool (search) and click Shell Extensions. In the list on the right find 'Appindicator support' and click the config icon next to the on/off toggle. Change the 'default place for indicator icons' to 'show in panel' and leave all the options as default.

Pre-requisites
--------------

- Python 2 (tested in 2.7.5)
- feedparser 5.1.3 (linux) 5.1.2 (windows)
- PyQT (4.x)

Linux setup
-----------

- Todo

Windows setup
-------------

- python setuptools
  - download: https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py
  - run python ez_setup.py
- cx_freeze
  - grab the installer from here: http://www.lfd.uci.edu/~gohlke/pythonlibs/

- feedparser
  - Note! dont use 5.1.3 otherwise you'll get a urllib.request import error. I dropped back to 5.1.2 and it works ok
  - download: https://pypi.python.org/packages/source/f/feedparser
  - unzip and go into directory
  - run python setup.py install


Python 3 notes (not currently in use)

* sudo apt-get install python3-setuptools (gives you easy_install3 on the command line)
* sudo easy_install3 pip (gives you pip-3.2 on the command line)
* sudo pip-3.2 install feedparser


Build
-----
- python setup.py build
- python setup_windows.py build


Install
-------
- linux = sudo python setup.py install --prefix=/usr/local


Run
---
- ./ccnotifier (linux)
- ccnotifier.exe (windows)
