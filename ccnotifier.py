#!/usr/bin/python

import sys
import webbrowser

from db.AppDb import ConfigQuery
from ui.pyqt.AppConfigPyqt import AppConfigPyqt  as AppConfig
from ui.pyqt.AppIndicatorPyqt import AppIndicatorPyqt as AppIndicator

class ccNotifier:

    db = None
    indicator = None
    user_id = None
    ui = None
    eventtimerId = None

    def __init__(self):
        self.db = ConfigQuery()
        self.db.createDb()
        self.ui = AppConfig(self)
        self.indicator = AppIndicator(self)

    def showConfig(self, test):
        self.ui.build()

    def reshowConfig(self, test):
        self.ui.show()

    def hideConfig(self):
        self.ui.hide()

    def getConfig(self):
        return self.db.get_config()

    def setUser(self, data):
        formData = data()
        ConfigQuery().set_user(formData)
        self.runTimer()
        self.hideConfig()

    def runTimer(self, widget=None):
        self.ui.startCheckTimer()

    def showBrowser(self, item, link):
        if isinstance(item, basestring):
            finallink = item
        else:
            finallink = link
        webbrowser.open(finallink)

    def quit(self, test):
        self.ui.quit()
        self.indicator.quit()


if hasattr(sys, "frozen") or hasattr(sys, "importers") or hasattr(sys, "__main__"):
    # running a compiled/frozen version
    pass
else:
    # running the source py files as a developer
    pass

App = ccNotifier()
App.showConfig('test')