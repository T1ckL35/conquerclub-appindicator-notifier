#!/usr/bin/env python

from cx_Freeze import setup, Executable

buildOptions = dict(
	packages = [
        "sqlite3",
        "db",
        "ui"
	],
	include_files = [
		"db/schema.sql",
        "ui/img/cc.ico",
        "ui/img/cc2.ico",
	],
	excludes = [
        "gi",
        "gtk",
        "gobject",
        "glib",
		"tcl",
		"tkk",
		"_osx_support",
		"bz2",
        "email",
        "unittest",
        "xml"
	]
)

executables = [
    Executable(
        script = "ccnotifier.py",
    	targetName = "ccnotifier",
    	compress = True,
    	icon = None,
    	appendScriptToExe = True,
    	appendScriptToLibrary = True,
    )
]

setup(
	name='ccnotifier',
    version = '1.0',
    author = "Simon Mann, simon.mann+ccnotifer@gmail.com",
    description = 'Cross platform desktop notifier for Conquer Club (wwww.conquerclub.com)',
    options = dict( build_exe = buildOptions ),
    executables = executables
)