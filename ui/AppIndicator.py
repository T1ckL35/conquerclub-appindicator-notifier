
from conquerclub_reader import ConquerClubReader

class AppIndicator:
    eventtimer = 0
    base_url = 'http://www.conquerclub.com'
    indicator = None
    
    def __init__(self, app):
        self.app = app

    def getStatus(self):
        return ConquerClubReader().getRecords(self.app.getConfig())

    def quit(self):
        pass
        