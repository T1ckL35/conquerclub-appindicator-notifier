
class AppConfig:
    
    app = None
    window = None
    window_title = 'CCNotifier'
     # container
    vbox = None
    eventtimerId = None
    quit = None

    # ui components
    user_label = None
    text_entry = None
    password_label = None
    password_entry = None
    eliminated_entry = None
    polling_label = None
    polling_entry = None

    def __init__(self, app):
        self.app = app

    def __loadConfig(self):
        pass

    def build(self):
        self.buildContent() #wrapper
        self.buildUser()
        # self.buildPassword()
        self.buildEliminated()
        self.buildPolling()
        self.buildSubmit()
        self.populateContent() # base level
        self.showWindow()

    def buildContent(self):
        pass

    # username/id label/input
    def buildUser(self):
        pass

    # password label/input
    def buildPassword(self):
        pass

    # show eliminated checkbox
    def buildEliminated(self):
        pass

    # polling duration label/input
    def buildPolling(self):
        pass

    # get the db values and populate the ui
    def populateContent(self):
        user_record = self.app.getConfig()

        formText = "1"
        if user_record.user_id:
            formText = user_record.user_id
        self.addEntryText(self.text_entry, formText)
        if user_record.password and self.password_entry:
            self.addEntryText(self.password_entry, user_record.password)

        self.addCheckboxValue(self.eliminated_entry, user_record.show_eliminated)

        polling_interval = '5'
        if user_record.polling_interval:
            polling_interval = user_record.polling_interval
        self.addEntryText(self.polling_entry, polling_interval)

    # ui specific method for filling textfields in
    def addEntryText(self, item, text):
        pass

    # ui specific method for checkboxes
    def addCheckboxValue(self, item, value):
        pass

    # grabs the current form values 
    def getformValues(self):
        show_eliminated = '0'
        if self.getCheckboxState(self.eliminated_entry):
            show_eliminated = '1'
        password = ''
        if self.password_entry:
            password = self.getEntryText(self.password_entry)
        return {
            'user_id' : self.getEntryText(self.text_entry),
            'password' : password,
            'show_eliminated' : show_eliminated,
            'polling_interval' : self.getEntryText(self.polling_entry)
        }

    def sendUserDetails(self, item):
        self.app.setUser(self.getformValues)

    def getEntryText(self, item):
        pass

    def getCheckboxState(self, item):
        pass

    # submit button and action
    def buildSubmit(self):
        pass

    # starts ui specific timer to check the RSS feed
    def startcheckTimer(self):
        pass

    def showWindow(self):
        pass

    def show(self):
        self.window.show()

    def hide(self):
        self.window.hide()

    def quit(self):
        pass
        