import os
import sys
from PyQt4 import QtCore, QtGui

from functools import partial

from ui.AppIndicator import AppIndicator
from ui.pyqt.AppNotifierPyqt import AppNotifierPyqt as AppNotifier

class AppIndicatorPyqt(AppIndicator):

    menu = None
    
    def __init__(self, app):
        AppIndicator.__init__(self, app)
        self.qt_widget = SystrayWidget()
        self.buildIndicator()

    def buildIndicator(self):
        self.tray = QtGui.QSystemTrayIcon(self.qt_widget)
        self.tray.setIcon(self.setActiveIcon())
        self.notifier = AppNotifier(self.tray)

    def setActiveIcon(self):
        return QtGui.QIcon(os.getcwd()+"/ui/img/cc.ico")

    def setAttentionIcon(self):
        return QtGui.QIcon(os.getcwd()+"/ui/img/cc2.ico")

    # self.tray.hide() =  linux specific thing - you MUST hide the parent of the menu before we can call clear()
    def checkStatus(self, user_id):
        data, attention = self.getStatus()

        if not self.menu:
            self.menu = QtGui.QMenu("GameMenu")
        else:
            self.tray.hide()
            self.menu.clear()

        if attention == True:
            self.tray.setIcon(self.setAttentionIcon())
            self.notifier.gameReady()
        else:
            self.tray.setIcon(self.setActiveIcon())

        self.menu.addAction('CCNotifier')
        conf = self.menu.addAction('Config')
        conf.triggered.connect(self.app.reshowConfig)

        for entry in data:
            game_item = self.menu.addAction(entry.title)
            if 'READY' in entry.title:
                game_item.setIcon(self.setAttentionIcon())
            else:
                game_item.setIcon(self.setActiveIcon())
            game_item.triggered.connect(partial(self.app.showBrowser, entry.link))

        force = self.menu.addAction('Force check')
        force.triggered.connect(self.app.runTimer)
        exitAction = self.menu.addAction("Quit")
        exitAction.triggered.connect(self.quit)

        self.tray.setContextMenu(self.menu)
        self.tray.show()

    def quit(self):
        self.app.ui.quit()
        
class SystrayWidget(QtGui.QWidget):
    def __init__(self):
        QtGui.QWidget.__init__(self)