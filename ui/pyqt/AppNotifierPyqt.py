from PyQt4 import QtCore, QtGui
from ui.AppNotifier import AppNotifier

class AppNotifierPyqt(AppNotifier):

    def __init__(self, indicator):
        self.indicator = indicator

    def gameReady(self):
        if self.indicator.supportsMessages():
            title, message = self.gameReadyMessage()
            self.indicator.showMessage(title, message, QtGui.QSystemTrayIcon.Information) #, 10000)

