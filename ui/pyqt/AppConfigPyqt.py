import sys
from PyQt4 import QtCore, QtGui

from ui.AppConfig import AppConfig

class AppConfigPyqt(AppConfig):

    qt_app = None

    def __init__(self, app):
        AppConfig.__init__(self, app)
        self.qt_app = QtGui.QApplication(sys.argv)
        self.window = QtGui.QWidget()
        self.window.setWindowTitle(self.window_title)
        self.window.setGeometry(200, 300, 200, 150)

    def buildContent(self):
        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addStretch(1)

    # username/id label/input
    def buildUser(self):
        self.user_label = QtGui.QLabel("Username/Id", self.window)
        self.text_entry = QtGui.QLineEdit()
        self.vbox.addWidget(self.user_label)
        self.vbox.addWidget(self.text_entry)

    # password label/input
    def buildPassword(self):
        self.password_label = QtGui.QLabel("Website Password", self.window)
        self.password_entry = QtGui.QLineEdit()
        self.vbox.addWidget(self.password_label)
        self.vbox.addWidget(self.password_entry)

    # show eliminated checkbox
    def buildEliminated(self):
        self.eliminated_entry = QtGui.QCheckBox('Show eliminated games', self.window)
        self.vbox.addWidget(self.eliminated_entry)

    # polling duration label/input
    def buildPolling(self):
        self.polling_label = QtGui.QLabel("Minutes between game check", self.window)
        self.polling_entry = QtGui.QLineEdit()
        self.vbox.addWidget(self.polling_label)
        self.vbox.addWidget(self.polling_entry)

    def buildSubmit(self):
        self.button = QtGui.QPushButton('Submit', self.window)
        self.button.resize(self.button.sizeHint())
        self.button.move(0, 25)
        self.button.clicked.connect(self.sendUserDetails)
        self.vbox.addWidget(self.button)

    # ui specific method for filling textfields in
    def addEntryText(self, item, text):
        item.setText(text)

    # ui specific method for checkboxes
    def addCheckboxValue(self, item, value):
        if value == '1':
            item.setCheckState(QtCore.Qt.Checked)

    def getEntryText(self, item):
        return item.text()

    def getCheckboxState(self, item):
        return item.isChecked()

    def startCheckTimer(self):
        if not self.eventtimerId:
            # convert number to an integer and to milliseconds
            time_check = int(self.getEntryText(self.polling_entry)) * 1000 * 60
            self.eventTimer = QtCore.QTimer()
            QtCore.QObject.connect(self.eventTimer, QtCore.SIGNAL("timeout()"), self.callCheckStatus)
            # fire off the event immediately
            self.callCheckStatus()
            # get check time from the config to start the timer checks
            self.eventTimer.start(time_check)

    def callCheckStatus(self):
        self.app.indicator.checkStatus(self.getEntryText(self.text_entry))

    def runNotifier(self, element):
        pass

    def showWindow(self):
        self.window.setLayout(self.vbox)
        self.show()
        sys.exit(self.qt_app.exec_())

    def show(self):
        self.window.show()

    def hide(self):
        self.window.hide()

    def quit(self):
        self.qt_app.quit()
